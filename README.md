# OpenML dataset: Worldwide-Meat-Consumption

https://www.openml.org/d/43758

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Meat consumption is related to living standards, diet, livestock production and consumer prices, as well as macroeconomic uncertainty and shocks to GDP. Compared to other commodities, meat is characterised by high production costs and high output prices. Meat demand is associated with higher incomes and a shift - due to urbanisation - to food consumption changes that favour increased proteins from animal sources in diets. While the global meat industry provides food and a livelihood for billions of people, it also has significant environmental and health consequences for the planet.
This dataset was refreshed in 2018, with world meat projections up to 2026 are presented for beef and veal, pig, poultry, and sheep. Meat consumption is measured in thousand tonnes of carcass weight (except for poultry expressed as ready to cook weight) and in kilograms of retail weight per capita. Carcass weight to retail weight conversion factors are: 0.7 for beef and veal, 0.78 for pig meat, and 0.88 for both sheep meat and poultry meat. Excludes Iceland but includes all EU 28 member countries.
Content
The csv file has 5 columns:

LOCATION = the country code name
SUBJECT = The type of meat(pig, beef, etc)
TIME = the year the data was recorded
MEASURE = the measure used to show the value
VALUE = The value, according to the measure

Acknowledgements
https://data.oecd.org/agroutput/meat-consumption.htm
OECD/FAO (2017), OECD-FAO Agricultural Outlook, OECD Agriculture statistics (database). doi: dx.doi.org/10.1787/agr-outl-data-en (Accessed on 24 January 2018)
Inspiration
Ask questions such as:

What country had the most meat consumption?
What years had the most worldwide meat consumption?
Is there any correlation between the type of meat and the amount being consumed?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43758) of an [OpenML dataset](https://www.openml.org/d/43758). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43758/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43758/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43758/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

